﻿using UnityEngine;
using System.Collections;

public class BoltController : MonoBehaviour {
	public float speed;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();

		rigidbody.velocity = transform.up * speed;
	}
}
