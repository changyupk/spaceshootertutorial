﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody> ();

		rigidbody.velocity = transform.up;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
