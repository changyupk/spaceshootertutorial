﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

//	public GameObject bullet;
//	public Transform bulletSpawnPoint;
	public GameObject bolt;
	public Transform boltSpawnPoint;

	private Rigidbody rigidbody;
	public float speed;

	private float nextFire;
	public float fireRate;


	void Start () {
		rigidbody = GetComponent<Rigidbody> ();
		nextFire = Time.time;
	}

	void Update() {
		if (Input.GetButton ("Fire1") && Time.time > nextFire) {
			Debug.Log ("Fired at " + Time.time);
			nextFire = Time.time + fireRate;

//			Instantiate (bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
			Instantiate (bolt, boltSpawnPoint.position, bolt.transform.rotation);
		}
	}

	void FixedUpdate () {
		float x = Input.GetAxis ("Horizontal");
		float z = Input.GetAxis ("Vertical");

		rigidbody.velocity = Vector3.right * x * speed + Vector3.forward * z * speed;
		rigidbody.position = new Vector3 (Mathf.Clamp (rigidbody.position.x, -6.0f, 6.0f),
			rigidbody.position.y,
			Mathf.Clamp (rigidbody.position.z, -4.0f, 10.0f));

		rigidbody.rotation = Quaternion.Euler (0, 0, -x * 30);
	}
}
