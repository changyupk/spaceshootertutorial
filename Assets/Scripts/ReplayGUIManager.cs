﻿using System;

using UnityEngine;
using UnityEngine.UI;
using Swing;
using Swing.ReplayEngine;

public class ReplayGUIManager : MonoBehaviour
{
	// button UI controls
	GameObject recordButtonObject, playbackButtonObject, stopButtonObject,
				pauseButtonObject, resumeButtonObject, broadcastButtonObject, watchButtonObject,
				listReplaysButtonObject, listBroadcastsButtonObject;
	Button recordButton, playbackButton, stopButton,
				pauseButton, resumeButton, broadcastButton, watchButton,
				listReplaysButton, listBroadcastsButton;

	// status UI control
	GameObject statusTextObject;
	Text statusText;

	// target UI controls
	GameObject playbackTargetTextObject, watchTargetTextObject;
	Text playbackTargetText, watchTargetText;

	// list UI control
	GameObject listTextObject;
	Text listText;

	// Replayer
	public ReplayStore store;
	Replayer replayer = null;
	Replay[] replays = null;


	void Awake() {
		// UI

		recordButtonObject = GameObject.Find ("Record Button");
		playbackButtonObject = GameObject.Find ("Playback Button");
		stopButtonObject = GameObject.Find ("Stop Button");
		pauseButtonObject = GameObject.Find ("Pause Button");
		resumeButtonObject = GameObject.Find ("Resume Button");
		broadcastButtonObject = GameObject.Find ("Broadcast Button");
		watchButtonObject = GameObject.Find ("Watch Button");
		listReplaysButtonObject = GameObject.Find ("List Replays Button");
		listBroadcastsButtonObject = GameObject.Find ("List Broadcasts Button");

		recordButton = recordButtonObject.GetComponent<Button> ();
		playbackButton = playbackButtonObject.GetComponent<Button> ();
		stopButton = stopButtonObject.GetComponent<Button> ();
		pauseButton = pauseButtonObject.GetComponent<Button> ();
		resumeButton = resumeButtonObject.GetComponent<Button> ();
		broadcastButton = broadcastButtonObject.GetComponent<Button> ();
		watchButton = watchButtonObject.GetComponent<Button> ();
		listReplaysButton = listReplaysButtonObject.GetComponent<Button> ();
		listBroadcastsButton = listBroadcastsButtonObject.GetComponent<Button> ();

		recordButton.onClick.AddListener (OnRecord);
		playbackButton.onClick.AddListener (OnPlayback);
		stopButton.onClick.AddListener (OnStop);
		pauseButton.onClick.AddListener (OnPause);
		resumeButton.onClick.AddListener (OnRecord);
		broadcastButton.onClick.AddListener (OnBroadcast);
		watchButton.onClick.AddListener (OnWatch);
		listReplaysButton.onClick.AddListener (OnListReplays);
		listBroadcastsButton.onClick.AddListener (OnListBroadcasts);

		statusTextObject = GameObject.Find ("Status");
		statusText = statusTextObject.GetComponent<Text> ();

		playbackTargetTextObject = GameObject.Find ("Playback Target Text");
		watchTargetTextObject = GameObject.Find ("Watch Target Text");

		playbackTargetText = playbackTargetTextObject.GetComponent<Text> ();
		watchTargetText = watchTargetTextObject.GetComponent<Text> ();

		listTextObject = GameObject.Find ("List Text");
		listText = listTextObject.GetComponent<Text> ();
			
		// Replayer

		replayer = Replayer.GetInstance ();
		if (replayer == null) {
			Debug.LogError ("Can't instantiate the Replayer.");
		} else {
			Guid uid = new Guid ();
			replayer.Init (uid);
		}
	}

	void OnGUI() {
//		Debug.Log ("OnGUI" + replayer.GetReplayMode());

		UpdateStatusText ();
	}

	void UpdateStatusText() {
		statusText.text = "Current: " + ((replayer.currentReplay == null) ? "null" : replayer.currentReplay.id.ToString())
			+ "\nState: " + replayer.replayMode
			+ "\nStore: " + store;
	}

	void LogReplay(Replay replay) {
		Debug.Log ("Replay" +
			"\n\tid = " + replay.id +
			"\n\tcreated = " + replay.createdTime);
	}


	public void OnRecord() {
		ReplayMode state = replayer.replayMode;

		if (state == ReplayMode.Idle) {
			Debug.Log ("Start recording");

			Replay replay = replayer.Record ((e) => {
				if (e != null) {
					Debug.Log("An error occured during recording. " + e);
					return;
				}

				Debug.Log("Recording ended.");
			});

			if (replay == null) {
				Debug.Log ("Can't start recording");
			}

			LogReplay (replayer.currentReplay);
		} else {
			Debug.Log ("Can't record; invalid state, " + state);
		}
	}

	public void OnPlayback() {
		ReplayMode state = replayer.replayMode;
		if (state == ReplayMode.Idle) {
			Debug.Log ("Start to playback " + playbackTargetText.text);

			int target;
			Replay replay;

			if (playbackTargetText.text != string.Empty) {
				try {
					target = int.Parse (playbackTargetText.text);
				} catch (Exception e) {
					Debug.Log ("Can't parse the playback target, " + playbackTargetText.text + ", into a number.");
					return;
				}

				if (replays == null) {
					Debug.Log ("There is no replay list.");
					return;
				} else if (target >= 0 && target < replays.Length) {
					replay = replays [target];
				} else {
					Debug.Log ("The target replay, " + target + ", does not exist in the replay list.");
					return;
				}
			} else {
				if (replayer.currentReplay != null) {
					replay = replayer.currentReplay;
				} else {
					Debug.Log ("The CWR is null.");
					return;
				}
			}

			if (false == replayer.LoadReplay (replay, (e) => {
				if (e != null) {
					Debug.Log ("An error occured during loading a replay. " + e);
					return;
				}

				Debug.Log ("Loaded the replay.");

				if (false == replayer.Playback ((ex) => {
					if (ex != null) {
						Debug.Log ("An error occured during playback. " + ex);
						return;
					}

					Debug.Log ("Playback ended.");
				})) {
					Debug.Log ("Can't start playback of a replay.");
				}
			})) {
				Debug.Log ("Can't start loading a replay.");	
			}
		} else {
			Debug.Log ("Can't playback; invalid state, " + state);
		}
	}

	public void OnStop() {
		ReplayMode state = replayer.replayMode;

		if (state == ReplayMode.Recording) {
			Debug.Log ("Stop recording");

			replayer.Stop ();

			bool result = replayer.SaveReplay (store, (e) => {
				if (e != null) {
					Debug.Log("Can't save the replay to the " + store + " (1)");
					return;
				}

				Debug.Log("Saved the replay to the " + store);
			});

			if (result == false) {
				Debug.Log ("Can't save the replay to the " + store + " (2)");
			}
		} else if (state == ReplayMode.PlayingBack) {
			Debug.Log ("Stop playback");

			replayer.Stop ();
		} else {
			Debug.Log ("Can't stop; invalid state, " + state);
		}
	}

	public void OnPause() {
		if (replayer.replayMode == ReplayMode.PlayingBack) {
			Debug.Log ("Pause to playback");

			replayer.Pause();
		} else {
			Debug.Log ("Can't pause; invalid state, " + replayer.replayMode);
		}
	}

	public void OnResume() {
		//TBD: ReplayMode.Paused not exist.
//		if (replayer.GetReplayMode() == ReplayMode.Paused) {
//			Debug.Log ("Resume to playback");
//
//			if (replayer.PlayBack (replayer.currentReplay, false, Swing.ReplayEngine.ReplayOptions.Nothing) == true) {
//			} else {
//				Debug.Log ("Can't resume");
//			}
//		} else {
//			Debug.Log ("Can't resume; invalid state, " + state);
//		}
	}

	public void OnBroadcast() {
	}

	public void OnWatch() {
	}

	public void OnListReplays() {
		Debug.Log ("List replays");

		replayer.ListReplays (store, 0, 10, (e, replays, total) => {
			if (e != null) {
				Debug.Log(e);
				return;
			}

			Debug.Log(replays.Length + " replays are listed.");

			string list = "";

			for (int i = 0; i < replays.Length; i++) {
				Replay replay = replays[i];

				list += i + ". " + replay.id + "\n";
			}

			listText.text = list;
			Debug.Log(list);

			this.replays = replays;
		});
	}

	public void OnListBroadcasts() {
		Debug.Log ("List broadcasts");

		replayer.ListBroadcasts (0, 10, (e, broadcasts, total) => {
			if (e != null) {
				Debug.Log(e);
				return;
			}

			Debug.Log(broadcasts.Length + " broadcasts are listed.");

			string list = "";

			for (int i = 0; i < broadcasts.Length; i++) {
				Broadcast broadcast = broadcasts[i];

				list += i + ". " + broadcast.id + "\n";
			}

			listText.text = list;
			Debug.Log(list);

			this.replays = replays;
		});
	}
}


